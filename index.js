const botaoEnviar = document.getElementById('botao-enviar');

const formulario = document.getElementById('formulario-assinatura');
botaoEnviar.addEventListener( 'click', () => {

    const campoEmail = document.getElementById('campo-email');
    
    const conteudoCampoEmail = campoEmail.value;
    if( conteudoCampoEmail.indexOf('@') >= 1 ) {
        formulario.submit();
    } else {
        alert('Confira o campo e-mail.');
    }
} 
);


